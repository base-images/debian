#!/bin/bash
set -o errexit -o nounset -o pipefail

trap_handler() {
  local sig=$1
  echo "TRAPed signal: $sig"
  exit 1
}
for sig in HUP INT QUIT TERM; do
  trap 'trap_handler '"$sig"'' "$sig"
done

# If you need to run any command as root before execute your CMD
if [ -x /usr/local/sbin/run.sh ]; then
  # shellcheck source=/dev/null
  source /usr/local/sbin/run.sh
fi

# If $RUN_USER is set, change /opt permitions to $RUN_USER
if [ -n "${RUN_USER:-}" ]; then
  if [ -z "${RUN_GROUP:-}" ]; then
    RUN_GROUP=$(id -gn "${RUN_USER}")
    export RUN_GROUP
  fi
  chown -R "${RUN_USER}:${RUN_GROUP}" /opt
  sync;
else
  RUN_USER="$(id -un)"
  RUN_GROUP="$(id -gn)"
  export RUN_USER RUN_GROUP
fi

exec gosu "${RUN_USER}" "${@}"
