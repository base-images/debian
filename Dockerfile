FROM debian:12

LABEL maintainer="Felipe Lemos <felipelemos@gmail.com>"

ENV DEBIAN_FRONTEND=noninteractive \
    RUN_USER=appuser \
    RUN_USER_ID=911 \
    TZ=Europe/Warsaw \
    LANG=en_US.UTF-8

COPY files/entrypoint.sh /usr/local/sbin/entrypoint.sh

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
    bash \
    ca-certificates \
    gosu \
    locales \
 && gosu nobody true \
 && sed -i "s/^# *\(${LANG}\)/\1/" /etc/locale.gen \
 && locale-gen "${LANG}" \
 && update-locale LANG="${LANG}" \
 && update-locale LANGUAGE="${LANG}" \
 && update-locale LC_ALL="${LANG}" \
 && groupadd -g "${RUN_USER_ID}" "${RUN_USER}" \
 && useradd -l -u "${RUN_USER_ID}" -g "${RUN_USER_ID}" -M -s /sbin/nologin "${RUN_USER}" \
 && chmod +x /usr/local/sbin/entrypoint.sh \
 && apt-get purge -y --auto-remove \
 && apt-get autoremove \
 && apt-get clean \
 && rm -rf /tmp/* \
 && rm -rf /var/lib/apt/lists/*

ONBUILD ENTRYPOINT ["/usr/local/sbin/entrypoint.sh"]
ONBUILD CMD ["/bin/bash"]
ENTRYPOINT ["/usr/local/sbin/entrypoint.sh"]
CMD ["/bin/bash"]
